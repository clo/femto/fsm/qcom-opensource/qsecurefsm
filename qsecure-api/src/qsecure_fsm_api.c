/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundatoin, nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/********************************************************************
QSCUREFSM API
*********************************************************************/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <linux/qseecom.h>
#include <linux/msm_ion.h>
#include "qsecure_fsm_api.h"
#include <sys/mman.h>

#ifdef __cplusplus
extern "C" {
#endif

#define QSEOS_VERSION_13	0x13
#define QSEOS_VERSION_14	0x14

#ifndef UINT_MAX
#define UINT_MAX 0xFFFFFFFF
#endif

#define QSEECOM_ALIGN_SIZE  0x40
#define QSEECOM_ALIGN_MASK  (QSEECOM_ALIGN_SIZE - 1)
#define QSEECOM_ALIGN(x)	\
	((x + QSEECOM_ALIGN_SIZE) & (~QSEECOM_ALIGN_MASK))

#define MSG_LOG fprintf

#ifdef QSEC_DEBUG
#define LOGD(fmt, args...) MSG_LOG(stdout, fmt, ##args)
#else
#define LOGD(fmt, args...)
#endif

#define LOGE(fmt, args...) MSG_LOG(stderr, fmt, ##args)

struct _QsecureFsm_handle {
	unsigned char *ion_sbuffer;
};

struct QsecureFsm_priv_handle {
	unsigned char *ion_sbuffer;
	uint32_t sbuf_len;
	int service_id;
	int qseecom_fd;
	int32_t ion_fd;
	int32_t ifd_data_fd;

	uint32_t  qseos_version;
	struct ion_handle_data ion_alloc_handle;
};

static int _QsecureFsm_dealloc_memory(struct _QsecureFsm_handle **handle,
							uint32_t ioctl_cmd);
static int _QsecureFsm_get_handle(struct _QsecureFsm_handle **clnt_handle, uint32_t sb_length);
static int _QsecureFsm_issue_send_service_cmd(struct _QsecureFsm_handle *qseecom_handle,
	void *send_buf, uint32_t sbuf_len, void *resp_buf, uint32_t rbuf_len,
								uint32_t cmd_id);


int _QsecureFsm_dealloc_memory(struct _QsecureFsm_handle **handle, uint32_t ioctl_cmd)
{
	struct QsecureFsm_priv_handle *priv_handle;
	struct ion_handle_data handle_data;
	int32_t ret;

	LOGD("QsecureFsm_dealloc_memory \n");
	if (handle == NULL) {
			LOGE("Error::Cannot de-alloc memory. handle is NULL!!.");
			return -1;
	}
	priv_handle = (struct QsecureFsm_priv_handle *) (*handle);
	if (priv_handle == NULL) {
			LOGE("Error::Cannot de-alloc memory. priv handle is NULL!!.");
			return -1;
	}

	if (priv_handle->qseos_version < QSEOS_VERSION_14) {
		LOGE("ERROR: UNLOAD_APP: Invalid qsee version =%d\n",
			priv_handle->qseos_version);
		return -1;
	}

	/* Deallocate the memory for the shared buffer */
	ret = munmap(priv_handle->ion_sbuffer,
	(priv_handle->sbuf_len+4095) & (~4095));
	if (ret)
			LOGE("Error: QsecureFsm_dealloc_memory munmap failed! wtih ret = %d", ret);
	handle_data.handle = priv_handle->ion_alloc_handle.handle;
	close(priv_handle->ifd_data_fd);
	close(priv_handle->qseecom_fd);
	ret = ioctl(priv_handle->ion_fd, ION_IOC_FREE, &handle_data);
	if (ret)
			LOGE("Error: Error in deallocating ION memory. ret = %d, errno = %d\n",
					ret, errno);
	close(priv_handle->ion_fd);
	free((struct QsecureFsm_priv_handle *)(*handle));
	*handle = NULL;
	return ret;
}


static int32_t _QsecureFsm_ION_memalloc(struct QsecureFsm_priv_handle *handle,
				uint32_t size)
{
	int32_t ret = 0;
	int32_t iret = 0;
	unsigned char *v_addr;
	struct ion_allocation_data ion_alloc_data;
	int32_t ion_fd;
	int32_t rc;
	struct ion_fd_data ifd_data;
	struct ion_handle_data handle_data;

	/* open ION device for memory management
	*/
	ion_fd  = open("/dev/ion", O_RDONLY);
	if (ion_fd < 0) {
		LOGE("Error::Cannot open ION device\n");
		return -1;
	}

	/* Size of allocation */
	ion_alloc_data.len = (size + 4095) & (~4095);

	/* 4K aligned */
	ion_alloc_data.align = 4096;

	/* memory is allocated from EBI heap */
	ion_alloc_data.flags = 0;
	ion_alloc_data.heap_id_mask  = ION_HEAP(ION_QSECOM_HEAP_ID);

	/* Set the buffers to be uncached */
	ion_alloc_data.flags = 0;

	/* IOCTL call to ION for memory request */
	rc = ioctl(ion_fd, ION_IOC_ALLOC, &ion_alloc_data);
	if (rc) {
		LOGE("Error::Error while trying to allocate data\n");
		ret = -1;
		goto alloc_fail;
	}

	if (ion_alloc_data.handle != (intptr_t)NULL) {
		ifd_data.handle = ion_alloc_data.handle;
	} else {
		LOGE("Error::ION alloc data returned a NULL\n");
		ret = -1;
		goto alloc_fail;
	}

	/* Call MAP ioctl to retrieve the ifd_data.fd file descriptor */
	rc = ioctl(ion_fd, ION_IOC_MAP, &ifd_data);
	if (rc) {
		LOGE("Error::Failed doing ION_IOC_MAP call\n");
		ret = -1;
		goto ioctl_fail;
	}

	/* Make the ion mmap call */
	v_addr = (unsigned char *)mmap(NULL, ion_alloc_data.len,
					PROT_READ | PROT_WRITE,
					MAP_SHARED, ifd_data.fd, 0);
	if (v_addr == MAP_FAILED) {
		LOGE("Error::ION MMAP failed\n");
		ret = -1;
		goto map_fail;
	}
	handle->ion_fd = ion_fd;
	handle->ifd_data_fd = ifd_data.fd;
	handle->ion_sbuffer = v_addr;
	handle->ion_alloc_handle.handle = ion_alloc_data.handle;

	return ret;

map_fail:
	iret = munmap(handle->ion_sbuffer, ion_alloc_data.len);
	if (iret)
		LOGE("Error::Failed to unmap memory for load image. ret = %d\n", ret);

ioctl_fail:
	handle_data.handle = ion_alloc_data.handle;
	if (handle->ifd_data_fd)
		close(handle->ifd_data_fd);
	iret = ioctl(ion_fd, ION_IOC_FREE, &handle_data);
	if (iret)
		LOGE("Error::ION FREE ioctl returned error = %d\n",iret);
alloc_fail:
	if (ion_fd) {
		close(ion_fd);
			handle->ion_fd = -1;
	}
	return ret;
}


int _QsecureFsm_get_handle(struct _QsecureFsm_handle **clnt_handle, uint32_t sb_length)
{
	struct qseecom_set_sb_mem_param_req mem_req;
	struct QsecureFsm_priv_handle *handle = NULL;
	struct qseecom_qseos_version_req version_req;
	struct ion_handle_data handle_data;
	int32_t ret = 0;
	int32_t iret = 0;

	LOGD("QsecureFsm_get_handle sb_length = 0x%x\n", sb_length);
	if (*clnt_handle != NULL) {
		LOGE("Error::Client handle is not null!!!\n");
		ret = -1;
		goto err_exit;
	}
	handle = malloc(sizeof(struct QsecureFsm_priv_handle));
	if (handle == NULL) {
		LOGE("Error::malloc failed. Could not allocate memory\n");
		ret = -1;
		goto err_exit;
	}
	memset(handle, 0, sizeof(struct QsecureFsm_priv_handle));

	handle->qseecom_fd = open("/dev/qseecom", O_RDWR);
	if (handle->qseecom_fd < 0) {
		LOGE("Error::Failed to open /dev/qseecom device\n");
		ret = -1;
		goto err_exit;
	}
	ret = ioctl(handle->qseecom_fd, QSEECOM_IOCTL_GET_QSEOS_VERSION_REQ,
					&version_req);
	if (ret) {
		LOGE("Error::QSEOS version info. ret value = %d", ret);
		goto err_exit;
	}

	handle->qseos_version = version_req.qseos_version;
	if (handle->qseos_version >= QSEOS_VERSION_14)
		sb_length += QSEECOM_ALIGN_SIZE *2;
	handle->sbuf_len = sb_length;

	/* Allocate Memory for the shared buffer */
	ret = _QsecureFsm_ION_memalloc(handle, sb_length);
	if (ret) {
		LOGE("Error::ION mem alloc failed with ret = %d", ret);
		goto err_exit;
	}

	memset(handle->ion_sbuffer, 0, sb_length);
	mem_req.sb_len = sb_length;
	mem_req.ifd_data_fd = handle->ifd_data_fd;
	mem_req.virt_sb_base = handle->ion_sbuffer;

	ret = ioctl(handle->qseecom_fd, QSEECOM_IOCTL_SET_MEM_PARAM_REQ,
			&mem_req);
	if (ret) {
		LOGE("Error::QSEECOM_IOCTL_SET_MEM_PARAM_REQ failed with ret = %d, errno = %d\n",
								ret, errno);
		iret = munmap(handle->ion_sbuffer,
				(handle->sbuf_len+4095) & (~4095));
		if (iret) {
			LOGE("Error::Failed to unmap memory for load image. ret = %d\n", ret);
			ret = iret;
		}

		handle_data.handle = handle->ion_alloc_handle.handle;
		close(handle->ifd_data_fd);
		handle->ifd_data_fd = -1;

		iret = ioctl(handle->ion_fd, ION_IOC_FREE, &handle_data);
		if (iret) {
			LOGE("Error::ION FREE ioctl returned error = %d\n",iret);
		   ret = iret;
		}
		close(handle->ion_fd);
		handle->ion_fd = -1;
		*clnt_handle = NULL;
		goto err_exit;
	}
	*clnt_handle = (struct _QsecureFsm_handle *)handle;

	return ret;

err_exit:
	if (handle != NULL) {
		if (handle->qseecom_fd)
			close(handle->qseecom_fd);
		free(handle);
	}
	return ret;
}

int _QsecureFsm_issue_send_service_cmd(struct _QsecureFsm_handle *qseecom_handle, void *send_buf,
		  uint32_t sbuf_len, void *resp_buf, uint32_t rbuf_len, uint32_t  cmd_id)
{
	struct qseecom_send_svc_cmd_req req = {0, NULL, 0, NULL, 0};
	int ret = 0;
	struct QsecureFsm_priv_handle *priv_handle;
	req.cmd_id = cmd_id;
	req.cmd_req_len = sbuf_len;
	req.resp_len = rbuf_len;

	priv_handle = (struct QsecureFsm_priv_handle *) qseecom_handle;

	if (priv_handle == NULL) {
		LOGE("priv handle is NULL!!.\n");
		return -1;
	}

	req.cmd_req_buf = priv_handle->ion_sbuffer;
	memcpy(priv_handle->ion_sbuffer, send_buf, sbuf_len);
	if ((sbuf_len & QSEECOM_ALIGN_MASK) &&
			(priv_handle->qseos_version == QSEOS_VERSION_14))
		req.cmd_req_len = QSEECOM_ALIGN(sbuf_len);
	if ((rbuf_len & QSEECOM_ALIGN_MASK) &&
				(priv_handle->qseos_version == QSEOS_VERSION_14))
		req.resp_len = QSEECOM_ALIGN(rbuf_len);

	if(req.cmd_req_len > priv_handle->sbuf_len) {
		LOGE("Error: cmd_req_len = %d > ion share buf_len = %d\n",
			req.cmd_req_len, priv_handle->sbuf_len);
		return ret;
	}
	req.resp_buf = priv_handle->ion_sbuffer + req.cmd_req_len;
	ret = ioctl(priv_handle->qseecom_fd, QSEECOM_IOCTL_SEND_CMD_SERVICE_REQ, &req);
	if (resp_buf) {
		if ((req.cmd_req_len > UINT_MAX - req.resp_len) ||
			(req.cmd_req_len + req.resp_len > priv_handle->sbuf_len)) {
			LOGE("Error::req.resp_len = %d >  available space= %d\n", req.resp_len,
				(priv_handle->sbuf_len - req.cmd_req_len));
			return ret;
		}
		memcpy(resp_buf, req.resp_buf, rbuf_len);
	}
	if (ret) {
		LOGE("Error::send service command ioctl failed. ret = %d, errno = %d\n",
			ret, errno);
		return ret;
	}
	return ret;
}

int Qsecurefsm_send_service_cmd(void *send_buf, uint32_t sbuf_len,
				void *resp_buf, uint32_t rbuf_len, enum QsecureFsm_command_id cmd_id)
{
	struct _QsecureFsm_handle *qseecom_handle = NULL;
	struct QsecureFsm_priv_handle *priv_handle = NULL;
	int ret = -1;
	int dummy_resp = 0;
	uint32_t buf_len = 0;

	switch(cmd_id) {
		case QSEECOM_FSM_IKE_REQ_RSP:
			if ( ((send_buf == NULL) || (sbuf_len == 0)) ||
				 ((resp_buf == NULL) || (rbuf_len == 0)) )
			{
				LOGE("Error::Invalid input parameters: send_buf = 0x%p, resp_buf = 0x%p,"
					"sbuf_len = %d, rbuf_len = %d",
					send_buf, resp_buf,
					sbuf_len, rbuf_len);
				goto err_exit;
			}
			buf_len = sbuf_len + rbuf_len;
			/* Send dummy value, as so to be compatible with end to end architecture */
			break;
		case QSEECOM_FSM_IKE_REQ:
			if ( (send_buf == NULL) || (sbuf_len == 0) ) {
				LOGE("Error::Invalid input parameters: send_buf = 0x%p,"
					"sbuf_len = %d",
					send_buf,
					sbuf_len);
					goto err_exit;
			}
			/* Send dummy value, as so to be compatible with end to end architecture */
			resp_buf = &dummy_resp;
			rbuf_len = sizeof(dummy_resp);
			buf_len = sbuf_len + rbuf_len;
			break;
		default:
			LOGE("Error: Unsupported CMD_ID %d",cmd_id);
			goto err_exit;
			break;
	}
	if(0 != _QsecureFsm_get_handle(&qseecom_handle, buf_len)) {
		LOGE("QsecureFsm_get_ion_handle failed, exiting\n");
		goto err_exit;
	}
	ret = _QsecureFsm_issue_send_service_cmd(qseecom_handle, send_buf, sbuf_len,
				resp_buf, rbuf_len, cmd_id);
	if (ret)
		LOGE("QsecureFsm_Send_service_cmd failed\n");

	 _QsecureFsm_dealloc_memory(&qseecom_handle, (uintptr_t) NULL);
err_exit:
	 if (qseecom_handle != NULL) {
		priv_handle = (struct QsecureFsm_priv_handle *) qseecom_handle;
		if (priv_handle->qseecom_fd)
			close(priv_handle->qseecom_fd);
		priv_handle = NULL;
		free(qseecom_handle);
	}
	return ret;
}


#ifdef __cplusplus
}
#endif
