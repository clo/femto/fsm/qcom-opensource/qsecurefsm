/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundatoin, nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/********************************************************************
QSECUREFSM Ike secure key management API
*********************************************************************/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <linux/msm_ion.h>
#include <time.h>
#include <sys/mman.h>
#include <getopt.h>
#include "qsecure_ike_api.h"
#include "qsecure_fsm_api.h"

tre_ike_request_cmd_t qseeReq;

int32_t tre_ike_encrypt_rsa_key(tre_ike_encrypt_key_cmd_t* req,
                               tre_ike_rsp_encrypt_key_cmd_t* rsp)
{
	int ret = 0;

	qseeReq.cmdId = QSEECOM_FSM_IKE_CMD_ENCRYPT_PRIVATE_KEY;
	memcpy((void *)&(qseeReq.req_buf),
			(void *)req, sizeof(tre_ike_encrypt_key_cmd_t));

	ret = Qsecurefsm_send_service_cmd((void *)&qseeReq,
				sizeof(tre_ike_request_cmd_t), (void *)rsp,
				sizeof(tre_ike_rsp_encrypt_key_cmd_t),
				QSEECOM_FSM_IKE_REQ_RSP);
	return ret;
}

int32_t tre_ike_prov_rsa_key(tre_ike_prov_key_cmd_t* req,
                             tre_ike_rsp_prov_key_cmd_t* rsp)
{
	int ret = 0;

	qseeReq.cmdId = QSEECOM_FSM_IKE_CMD_PROV_KEY;
	memcpy((void *)&(qseeReq.req_buf),
			(void *)req, sizeof(tre_ike_prov_key_cmd_t));

	ret = Qsecurefsm_send_service_cmd((void *)&qseeReq,
				sizeof(tre_ike_request_cmd_t), (void *)rsp,
				sizeof(tre_ike_rsp_prov_key_cmd_t),
				QSEECOM_FSM_IKE_REQ_RSP);
	return ret;
}

int32_t tre_ike_rsa_sign(tre_ike_sign_cmd_t* req,
                         tre_ike_rsp_sign_cmd_t* rsp)
{
	int ret = 0;

	qseeReq.cmdId = QSEECOM_FSM_IKE_CMD_SIGN;
	memcpy((void *)&(qseeReq.req_buf),
			(void *)req, sizeof(tre_ike_sign_cmd_t));

	ret = Qsecurefsm_send_service_cmd((void *)&qseeReq,
				sizeof(tre_ike_request_cmd_t), (void *)rsp,
				sizeof(tre_ike_rsp_sign_cmd_t),
				QSEECOM_FSM_IKE_REQ_RSP);
	return ret;
}

int32_t tre_ike_verify_rsa_signature(tre_ike_verify_sign_cmd_t* req,
                                     tre_ike_rsp_verify_sign_cmd_t* rsp)
{
	int ret = 0;

	qseeReq.cmdId = QSEECOM_FSM_IKE_CMD_VERIFY_SIGN;
	memcpy((void *)&(qseeReq.req_buf),
			(void *)req, sizeof(tre_ike_verify_sign_cmd_t));

	ret = Qsecurefsm_send_service_cmd((void *)&qseeReq,
				sizeof(tre_ike_request_cmd_t), (void *)rsp,
				sizeof(tre_ike_rsp_verify_sign_cmd_t),
				QSEECOM_FSM_IKE_REQ_RSP);
	return ret;

}


int32_t tre_ike_request_mac_for_CA_certificate(tre_ike_req_mac_cert_t* req,
                                               tre_ike_rsp_mac_cert_t* rsp)
{
	int ret = 0;

	qseeReq.cmdId = QSEECOM_FSM_IKE_CMD_REQ_MAC_ROOT_CA_CERT;
	memcpy((void *)&(qseeReq.req_buf),
			(void *)req, sizeof(tre_ike_req_mac_cert_t));

	ret = Qsecurefsm_send_service_cmd((void *)&qseeReq,
				sizeof(tre_ike_request_cmd_t), (void *)rsp,
				sizeof(tre_ike_rsp_mac_cert_t),
				QSEECOM_FSM_IKE_REQ_RSP);
	return ret;

}

int32_t tre_ike_verify_CA_certificate(tre_ike_verify_cert_t* req,
                                      tre_ike_rsp_verify_cert_t* rsp)
{
	int ret = 0;

	qseeReq.cmdId = QSEECOM_FSM_IKE_CMD_VERIFY_ROOT_CA_CERT;
	memcpy((void *)&(qseeReq.req_buf),
			(void *)req, sizeof(tre_ike_verify_cert_t));

	ret = Qsecurefsm_send_service_cmd((void *)&qseeReq,
				sizeof(tre_ike_request_cmd_t), (void *)rsp,
				sizeof(tre_ike_rsp_verify_cert_t),
				QSEECOM_FSM_IKE_REQ_RSP);
	return ret;
}
