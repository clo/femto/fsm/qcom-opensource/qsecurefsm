/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundatoin, nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*******************************************************************
QSECUREFSM IKE secure Key management API
*******************************************************************/

#ifndef __QSECURE_IKE_API
#define __QSECURE_IKE_API

#define MOD_SZ 512
#define PUB_EXP_SZ 256
#define PRIV_EXP_SZ 512
#define PAD_SZ 12

#define SIG_SZ MOD_SZ

#define MAX_PAD_INFO_LABEL 64
#define MAX_HASH_SIZE_BYTES 64
#define MAX_DATA_SIZE_BYTES 2048


enum IkeCmdId{
	QSEECOM_FSM_IKE_CMD_SIGN = 0x200,
	QSEECOM_FSM_IKE_CMD_PROV_KEY = 0x201,
	QSEECOM_FSM_IKE_CMD_ENCRYPT_PRIVATE_KEY = 0x202,
	QSEECOM_FSM_IKE_CMD_VERIFY_SIGN = 0x203,
	QSEECOM_FSM_IKE_CMD_REQ_MAC_ROOT_CA_CERT = 0x204,
	QSEECOM_FSM_IKE_CMD_VERIFY_ROOT_CA_CERT = 0x205
};


/* HLOS to send the RSA private key in this format*/
typedef struct {
	uint32_t 	nbits;		 	/* Number of bits in the modulus */
	char 		n[MOD_SZ];		/*  Modulus */
	char 		e[PUB_EXP_SZ];	/*  e Public exponent */
	char 		d[PRIV_EXP_SZ];	/*  Private exponent */
	char 		pad[PAD_SZ];	/*  Padding */
} rsa_key_t;


#define KEY_SZ sizeof(rsa_key_t)
/* Encrypted key needs 0x20 bytes for paddding */
#define ENC_KEY_SZ (KEY_SZ + 0x20)


/* RSA padding type. PKCS #1 v2.1 */
typedef enum {
	/*PKCS1 v1.5 signature*/
	CE_RSA_PAD_PKCS1_V1_5_SIG = 1,
	/*PKCS1 v1.5 encryption*/
	CE_RSA_PAD_PKCS1_V1_5_ENC = 2, /* not supported */
	/*OAEP Encryption*/
	CE_RSA_PAD_PKCS1_OAEP = 3,	   /* not supported */
	/*PSS Signature*/
	CE_RSA_PAD_PKCS1_PSS = 4,
	/* No Padding */
	CE_RSA_NO_PAD = 5,
} ce_rsa_padding_t;


/*index of hash algorithm used for generating signature */
typedef enum {
	CE_HASH_IDX_NULL = 1,
	CE_HASH_IDX_SHA1,
	CE_HASH_IDX_SHA256,
	CE_HASH_IDX_SHA384,
	CE_HASH_IDX_SHA512,
	CE_HASH_IDX_MAX
} ce_hash_idx_t;


typedef struct {
	ce_rsa_padding_t	padType;
	uint8_t				label[MAX_PAD_INFO_LABEL];  /* Label to add to msg */
	int32_t			 	labelLen;					/* Length of label */
} rsa_pad_info_t;


/* Request Structure- Encrypt Key */
typedef struct {
	/* buffer containing the key */
	rsa_key_t 	keybuf;
} tre_ike_encrypt_key_cmd_t;

/* Response Structure - Encrypt Key*/
typedef struct {
	/* Encrypt key result */
	int32_t 	result;
	/* Encrypted key buffer */
	uint8_t 	enc_keybuf[ENC_KEY_SZ];
} tre_ike_rsp_encrypt_key_cmd_t;

/* Request Structure- Prov Key */
typedef struct {
	/* buffer containing the encrypted key */
	uint8_t 	keybuf[ENC_KEY_SZ];
	/* flag to indicate the key is stored in fuse */
	bool 	key_in_fuse;
} tre_ike_prov_key_cmd_t;

/* Response Structure - Prov Key*/
typedef struct {
	int32_t 	result;
} tre_ike_rsp_prov_key_cmd_t;

/* Request Structure - Sign cmd */
typedef struct {
	/* Padding scheme to use for the signature:
	 * PKCS1v1.5 or PKCS1 PSS
	 */
	ce_rsa_padding_t 	rsa_pad;
	/* Salt length (only for PKCS1 PSS) */
	int32_t 			saltlen;
	/* length of the hash */
	uint32_t 			buflen;
	/* PKC buffer to sign */
	uint8_t 			buf[MAX_DATA_SIZE_BYTES];
} tre_ike_sign_cmd_t;

/* Response Structure - Sign cmd */
typedef struct {
	int 		result;
	uint32_t 	siglen;
	uint8_t 	buf[SIG_SZ];
} tre_ike_rsp_sign_cmd_t;

/* Request Structure - Verify Sign cmd */
typedef struct {
	rsa_key_t		public_key;	/* public key */
	uint32_t		pubkey_len;	/* public key length in bytes */
	ce_hash_idx_t	hashidx;
	rsa_pad_info_t	padding_info;
	uint8_t			data[MAX_DATA_SIZE_BYTES];
	uint32_t		data_len;
	uint8_t			signature[SIG_SZ];
	uint32_t		signature_len;
} tre_ike_verify_sign_cmd_t;

/* Response Structure - Verify Sign cmd */
typedef struct {
	int32_t		result;
} tre_ike_rsp_verify_sign_cmd_t;

/* Request Structure - Request MAC for Root Certificate cmd */
typedef struct {
	uint8_t		certificate[MAX_DATA_SIZE_BYTES];
	uint32_t	cert_len;
} tre_ike_req_mac_cert_t;

/* Response Structure - Return MAC for Root Certificate Rsp */
typedef struct {
	uint8_t		mac[MAX_HASH_SIZE_BYTES];
	uint32_t	mac_len;
} tre_ike_rsp_mac_cert_t;

/* Request Structure - Request Root Certificate verification cmd */
typedef struct {
	uint8_t		certificate[MAX_DATA_SIZE_BYTES];
	uint32_t	cert_len;
	uint8_t		mac[MAX_HASH_SIZE_BYTES];
	uint32_t	mac_len;
} tre_ike_verify_cert_t;

/* Response Structure - Return MAC for Root Certificate Rsp */
typedef struct {
	int		result;
} tre_ike_rsp_verify_cert_t;


typedef struct {
	int cmdId;
	union {
	  tre_ike_encrypt_key_cmd_t		c1;
	  tre_ike_prov_key_cmd_t		c2;
	  tre_ike_sign_cmd_t			c3;
	  tre_ike_verify_sign_cmd_t	 	c4;
	  tre_ike_req_mac_cert_t		c5;
	  tre_ike_verify_cert_t		 	c6;
	} req_buf;
} tre_ike_request_cmd_t;


typedef struct {
	int cmdId;
	union {
	  tre_ike_rsp_encrypt_key_cmd_t	r1;
	  tre_ike_rsp_prov_key_cmd_t	r2;
	  tre_ike_rsp_sign_cmd_t		r3;
	  tre_ike_rsp_verify_sign_cmd_t r4;
	  tre_ike_rsp_mac_cert_t		r5;
	  tre_ike_rsp_verify_cert_t		r6;
	} ret_buf;
} tre_ike_response_cmd_t;


int32_t tre_ike_encrypt_rsa_key(tre_ike_encrypt_key_cmd_t* req,
			tre_ike_rsp_encrypt_key_cmd_t* rsp);

int32_t tre_ike_prov_rsa_key(tre_ike_prov_key_cmd_t* req,
				tre_ike_rsp_prov_key_cmd_t* rsp);

int32_t tre_ike_rsa_sign(tre_ike_sign_cmd_t* req,
			tre_ike_rsp_sign_cmd_t* rsp);

int32_t tre_ike_verify_rsa_signature(tre_ike_verify_sign_cmd_t* req,
			tre_ike_rsp_verify_sign_cmd_t* rsp);

int32_t tre_ike_request_mac_for_CA_certificate(tre_ike_req_mac_cert_t* req,
			tre_ike_rsp_mac_cert_t* rsp);

int32_t tre_ike_verify_CA_certificate(tre_ike_verify_cert_t* req,
			tre_ike_rsp_verify_cert_t* rsp);

#endif

