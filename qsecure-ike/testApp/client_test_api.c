/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundatoin, nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/********************************************************************
QSECUREFSM Sample/Test Client app.
*********************************************************************/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <linux/msm_ion.h>
#include <time.h>
#include <sys/mman.h>
#include <getopt.h>
#include "qsecure_ike_api.h"

#define MSG_LOG fprintf
#define LOGE(fmt, args...) MSG_LOG(stderr, fmt, ##args)
#define LOGD(fmt, args...) MSG_LOG(stdout, fmt, ##args)

#define FAIL -1
#define OK    0

#define MAX_KEY_SIZE		4128
#define MAX_PATH_LEN		1024
#define FUSE_JTAGID_MASK	0xFFFFF


uint32_t strlcpy(char *dst, const char *src, size_t dstsize)
{
    uint32_t srclen = strlen(src);

    if (srclen < dstsize)
    {
        memcpy(dst, src, srclen);
        dst[srclen] = '\0';
    }
    else
    {
        memcpy(dst, src, dstsize-1);
        dst[dstsize-1] = '\0';
    }
    return srclen;
}

typedef struct _key_str {
	uint32_t nbits;
	const char *n;
	const char *e;
	const char *d;
	const char *pad;
} key_str_t;

typedef enum {FSM99XX, FSM90XX, BAD_TARGET} target_t;
target_t target;

rsa_key_t    _bin_key;
key_str_t   *_key = NULL;

tre_ike_request_cmd_t req;
tre_ike_response_cmd_t rsp;

/* Encrypted private key */
uint8_t ike_enc_key[ENC_KEY_SZ];
/* MAC for root CA cert */
tre_ike_rsp_mac_cert_t mac_info;
/* data to be signed and later verified */
tre_ike_sign_cmd_t sign_data;
/* Signature results of the RSA signing operation */
tre_ike_rsp_sign_cmd_t signature_info;

key_str_t _key_1024_fsm99xx = {
	1024,

	/* n */
	"9EC4D483330916B69EEE4E9B7614EAFC4FBF60E74B5127A3FF5BD9D48C7ECF8418D94D1E60388B"
	"B68546F8BC92DEB1974B9DEF6748FBB4EC93029EA8B7BEA36F61C5C6AEEDFD512A0F765846FAD5"
	"EDACB08C3D75CF1D43B48B394C94323C3F3E9BA6612F93FE2900134217433AFB088B5CA33FC4E6"
	"B270194DF077D2B6592743",

	/* e */
	"",

	/* d */
	"1A76236B332C2E73C527B7C493AE272A0D4A90268C8D869B5539F9A36CBFCD40AECEE22FBAB417"
	"4916367ECA187A72EE8C9A529136D49E276DD5C51C1E9FC5E7A265F1AF6D27E7A03311664CECDF"
	"4EE6230F59E1B4C5A0CF754334AE7B2CECCEFC65B1DDEE61319B76070F722795929E3D1868F89D"
	"C2B2DDF480220DF2A8368F",

	/* pad */
	""
};

key_str_t _key_2048_fsm99xx = {
	2048,

	/* n */
	"DEF570D66EA5CE366979768A42017B6706FD0525AD8EC90FDA187205F192F307976FFCCF5D1589"
	"D0DAA9C81958D188FC02AA8536C624B5482A6E6EAFF6F612C53D3490E4B3EE0D7889A6D82910BD"
	"9A86544CBDD936D6E661233348D82CCA08117F7EBF77F6596B3F662F8E0DD8A3402FAD8B12A2B0"
	"F4A0D65B455BEFC764F8371701E8B9010419417CBA7541E1490F24863B4BAE40D9B09C059ED073"
	"28625A88A7A02333A0E2066E93B21A339810652E11881FE8852CE86F2C698CBEF24AC769287BED"
	"863BFC39E9091A7523EF826BF632F073C2606AD59119F689112D380D033ADDA88951E416E87675"
	"727F8678387E14E80AD4A2DCC964D29E1D061049E1CD",

	/* e */
	"",

	/* d */
	"8E2C9712B62616F44D97FDC63646F9A8FE24D4666D6132C75FFCDCA0FBA608419C08F67E48CE56"
	"3E8F615ACF06FB3ECDECE94DDF5AC4C7B0C815F9914DFB26870DFB7A35CCA30D8602C918E83EDB"
	"9930F58AA6396025F36C97E4B91BD89BD03F503CC3DDBC82D2C7EFE27D444913F8F72E4413D318"
	"E235B9DB23C477877FD4632408E684E18D002AB137CF6BA97CB6161FDAB9F0EB0BF000115C5A17"
	"CA13D82E9EAA6141445675C667BFF335BAD8C72D07226FBFE70DF21D0557D4261A35C27DD0EF72"
	"36E367BFE1B4C4C81EC04E3429EB67ABBB43E722DABB6423F8B5A86E27B952D8940CBE5D437BDF"
	"028FB6517B59A47EBAE040D9E0293D546BF30DCEABE1",

	/* pad */
	""
};


key_str_t _key_1024_fsm90xx = {
	1024,

	/* n */
	"9EC4D483330916B69EEE4E9B7614EAFC4FBF60E74B5127A3FF5BD9D48C7ECF8418D94D1E60388B"
	"B68546F8BC92DEB1974B9DEF6748FBB4EC93029EA8B7BEA36F61C5C6AEEDFD512A0F765846FAD5"
	"EDACB08C3D75CF1D43B48B394C94323C3F3E9BA6612F93FE2900134217433AFB088B5CA33FC4E6"
	"B270194DF077D2B6592743",

	/* e */
	"3",

	/* d */
	"1A76236B332C2E73C527B7C493AE272A0D4A90268C8D869B5539F9A36CBFCD40AECEE22FBAB417"
	"4916367ECA187A72EE8C9A529136D49E276DD5C51C1E9FC5E7A265F1AF6D27E7A03311664CECDF"
	"4EE6230F59E1B4C5A0CF754334AE7B2CECCEFC65B1DDEE61319B76070F722795929E3D1868F89D"
	"C2B2DDF480220DF2A8368F",

	/*pad */
	""
};

key_str_t _key_2048_fsm90xx = {
	2048,

	/* n */
	"DEF570D66EA5CE366979768A42017B6706FD0525AD8EC90FDA187205F192F307976FFCCF5D1589"
	"D0DAA9C81958D188FC02AA8536C624B5482A6E6EAFF6F612C53D3490E4B3EE0D7889A6D82910BD"
	"9A86544CBDD936D6E661233348D82CCA08117F7EBF77F6596B3F662F8E0DD8A3402FAD8B12A2B0"
	"F4A0D65B455BEFC764F8371701E8B9010419417CBA7541E1490F24863B4BAE40D9B09C059ED073"
	"28625A88A7A02333A0E2066E93B21A339810652E11881FE8852CE86F2C698CBEF24AC769287BED"
	"863BFC39E9091A7523EF826BF632F073C2606AD59119F689112D380D033ADDA88951E416E87675"
	"727F8678387E14E80AD4A2DCC964D29E1D061049E1CD",

	/* e */
	"10001",

	/* d */
	"8E2C9712B62616F44D97FDC63646F9A8FE24D4666D6132C75FFCDCA0FBA608419C08F67E48CE56"
	"3E8F615ACF06FB3ECDECE94DDF5AC4C7B0C815F9914DFB26870DFB7A35CCA30D8602C918E83EDB"
	"9930F58AA6396025F36C97E4B91BD89BD03F503CC3DDBC82D2C7EFE27D444913F8F72E4413D318"
	"E235B9DB23C477877FD4632408E684E18D002AB137CF6BA97CB6161FDAB9F0EB0BF000115C5A17"
	"CA13D82E9EAA6141445675C667BFF335BAD8C72D07226FBFE70DF21D0557D4261A35C27DD0EF72"
	"36E367BFE1B4C4C81EC04E3429EB67ABBB43E722DABB6423F8B5A86E27B952D8940CBE5D437BDF"
	"028FB6517B59A47EBAE040D9E0293D546BF30DCEABE1",

	/* pad */
	"000000000000"
};


uint32_t hex_str_to_bin ( uint8_t* buf, uint32_t buflen,
                          const char *str, uint32_t* total_len)
{

	uint32_t i = 0;
	uint32_t j = 0;
	char ch;
	int8_t v;

	if ((NULL == buf) || (NULL == str) || (0 == buflen) || (NULL == total_len)) {
		LOGD("hex_str_to_bin failed (%d, %x)\n", buflen, (uint32_t)total_len);
		return -1;
	}

	memset(buf, 0, buflen);

	if ((strnlen(str, (((MAX_KEY_SIZE+7)/8)*2)+1)) & 1) {
		j = 1;
	}

	while (((ch = str[i]) != '\0') && ((j/2) < buflen)) {
		int tmp_ch_v = ch;
		if ((tmp_ch_v >= 0x30) && (tmp_ch_v <= 0x39)) {
			v = (unsigned char) ch - '0';
		}
		else if ((tmp_ch_v >= 0x41) && (tmp_ch_v <= 0x5a)) {
			v = (unsigned char) ch - 'A' + 10;
		}
		else if ((tmp_ch_v >= 0x61) && (tmp_ch_v <= 0x7a)) {
			v = (unsigned char) ch - 'a' + 10;
		}
		else {
			LOGD("hex_str_to_bin: unknown char tmp_ch_v=%x\n", tmp_ch_v);
			return -1;
		}

		++i;
		if (v >= 0 && v <= 0xf) {
			if ((j & 1) == 0) {
				buf[j / 2] = v << 4;
			}
			else {
				buf[j / 2] |= v;
			}
			++j;
		}
	}

	*total_len = (j / 2);

	return 0;

}

int32_t convert_key_str_to_bin(key_str_t* key_in, rsa_key_t* key_out)
{
	uint32_t n_len = 0;
	uint32_t e_len = 0;
	uint32_t d_len = 0;
	int ret =0;

	if ((ret = hex_str_to_bin((uint8_t *)key_out->n, sizeof(key_out->n),
                              (const char *)key_in->n, &n_len))) {
	   LOGD("hex to string on n failed (%d)\n", sizeof(key_out->n));
	   return ret;
	}

	if ((ret = hex_str_to_bin((uint8_t *)key_out->e, sizeof(key_out->e),
                              (const char *)key_in->e, &e_len))) {
	   LOGD("hex to string on e failed (%d)\n", sizeof(key_out->e));
	   return ret;
	}

	if ((ret = hex_str_to_bin((uint8_t *)key_out->d, sizeof(key_out->d),
                              (const char *)key_in->d, &d_len))) {
	   LOGD("hex to string on d failed (%d)\n", sizeof(key_out->d));
	   return ret;
	}

	memset(key_out->pad, 0, sizeof(key_out->pad));
	key_out->nbits = key_in->nbits;

	return ret;

}


target_t get_target (void)
{
	uint32_t jtag_id;
	char s[16];
	FILE *fp;
	const char *buildid_file = "/sys/devices/soc0/build_id";

	fp = fopen(buildid_file, "r");
	if (fp == NULL)  {
		fprintf(stderr, "%s: can't open %s\n", __func__, buildid_file);
		return BAD_TARGET;
	}
	fgets(s, 5, fp);
	jtag_id = (strtol(s, NULL, 16));
	fclose(fp);

	if ((jtag_id >= 0x9900) && (jtag_id <= 0x9955)) {
		LOGD("Testing on target FSM99XX (%x)\n", jtag_id);
		return FSM99XX;
	}
	else if ((jtag_id >= 0x9008) && (jtag_id <= 0x9055)) {
		LOGD("Testing on target FSM90XX (%x)\n", jtag_id);
		return FSM90XX;
	}
	else {
		LOGD("Unknown Target (%x)\n", jtag_id);
	}

	return BAD_TARGET;
}

int32_t load_rsa_key (char *keybuf)
{
	if (target == FSM99XX) {
		((rsa_key_t *)keybuf)->nbits = _key->nbits;

		memcpy((char *)(((rsa_key_t *)keybuf)->n),
				(char *)_key->n, sizeof(_bin_key.n));
		memcpy((char *)(((rsa_key_t *)keybuf)->e),
				(char *)_key->e, sizeof(_bin_key.e));
		memcpy((char *)(((rsa_key_t *)keybuf)->d),
				(char *)_key->d, sizeof(_bin_key.d));
		memcpy((char *)(((rsa_key_t *)keybuf)->pad),
				(char *)_key->pad, sizeof(_bin_key.pad));
	}
	else if (target == FSM90XX) {
		convert_key_str_to_bin(_key, &_bin_key);
		memcpy(keybuf, (char *)&_bin_key, KEY_SZ);
	}
	else {
		return FAIL;
	}

	return OK;
}


int32_t test_ike_encrypt_key(void)
{
	int ret = 0;

	if ((ret = load_rsa_key((char *)(&(req.req_buf.c1.keybuf))))) {
		LOGD("loading key failed\n");
		return ret;
	}

	ret = tre_ike_encrypt_rsa_key(&(req.req_buf.c1), &(rsp.ret_buf.r1));

	memcpy((char*)(ike_enc_key),
			(char*)(&(rsp.ret_buf.r1.enc_keybuf)), ENC_KEY_SZ);

	if(!ret)
		LOGD("IKE priv key encryption success\n");
	else
		LOGD("IKE priv key encryption failed (%d)\n", ret);
	return ret;
}

int32_t test_ike_prov_key(bool key_in_fuse)
{
	int ret = 0;

	if (key_in_fuse == false){
		memcpy((char *)(&(req.req_buf.c2.keybuf)),
			(char *)(ike_enc_key), ENC_KEY_SZ);
	}
	req.req_buf.c2.key_in_fuse = key_in_fuse;

	ret = tre_ike_prov_rsa_key(&(req.req_buf.c2), &(rsp.ret_buf.r2));

	if(!ret)
		LOGD("IKE priv key provision success\n");
	else
		LOGD("IKE priv key provision failed (%d)\n", ret);

	return ret;
}


int32_t test_ike_sign(void)
{
	int ret = 0;
	int i =0;
	req.req_buf.c3.rsa_pad = CE_RSA_PAD_PKCS1_V1_5_SIG;
	req.req_buf.c3.saltlen = 0;
	req.req_buf.c3.buflen = 0x20;
	for(i=0; i<32; i++){
		req.req_buf.c3.buf[i] = 0x34;
	}

	ret = tre_ike_rsa_sign(&req.req_buf.c3, &rsp.ret_buf.r3);

	if(!ret)
		LOGD("IKE Signature success \n");
	else
		LOGD("IKE RSA Signature Failed (%d)\n", ret);

	/* Save a copy of request msg to be used for verification test*/
	memcpy(&sign_data, &req.req_buf.c3,
			sizeof(tre_ike_sign_cmd_t));
	/* Save a copy of response msg to be used for verification test*/
	memcpy(&signature_info, &rsp.ret_buf.r3,
			sizeof(tre_ike_rsp_sign_cmd_t));
	return ret;
}


int32_t test_ike_verify_sign(void)
{
	int ret = 0;
	int i =0;
	int e_len;

	if (target == FSM99XX) {
		LOGD("Sorry, this feature is not supported for FSM99XX\n");
		return ret;
	}

	if ((ret = load_rsa_key((char *)(&req.req_buf.c4.public_key)))) {
		LOGD("loading key failed\n");
		return ret;
	}


	req.req_buf.c4.hashidx = CE_HASH_IDX_SHA256;
	req.req_buf.c4.padding_info.padType = CE_RSA_PAD_PKCS1_V1_5_SIG;
	req.req_buf.c4.padding_info.labelLen = 0;
	req.req_buf.c4.data_len = sign_data.buflen;

	/* Get public key length */
	if (((e_len = strnlen(_key->e, sizeof(_bin_key.e))) & 1)) {
		req.req_buf.c4.pubkey_len = ((e_len*4)/8) + 1;
	}
	else {
		req.req_buf.c4.pubkey_len = ((e_len*4)/8);
	}

	/* copy data */
	for(i=0; i<sign_data.buflen; i++){
		req.req_buf.c4.data[i] = sign_data.buf[i];
	}

	/* copy signature */
	req.req_buf.c4.signature_len = signature_info.siglen;
	for(i=0; i<signature_info.siglen; i++){
		req.req_buf.c4.signature[i] = signature_info.buf[i];
	}

	ret = tre_ike_verify_rsa_signature(&req.req_buf.c4, &rsp.ret_buf.r4);

	if(!ret)
		LOGD("IKE Signature verification command was successfuly recieved \n");
	else
		LOGD("IKE RSA Signature verification command Failed (%d)\n", ret);

	if(!rsp.ret_buf.r4.result)
		LOGD("RSA Signiture was verified\n");
	else
		LOGD("Failed to verify the RSA Signature (%d)\n", rsp.ret_buf.r4.result);

	return ret;
}



int32_t test_ike_req_mac_for_root_cert(void)
{
	int ret = 0;
	const char* cert = "This is a root CA certificate.";

	if (target == FSM99XX) {
		LOGD("Sorry, this feature is not supported for FSM99XX\n");
		return ret;
	}

	req.req_buf.c5.cert_len = strlen(cert) + 1;
	strlcpy((char *)(&req.req_buf.c5.certificate), cert, MAX_DATA_SIZE_BYTES);

	ret = tre_ike_request_mac_for_CA_certificate(&req.req_buf.c5, &rsp.ret_buf.r5);

	/* Save Mac for verification process */
	memcpy(&mac_info, &rsp.ret_buf.r5, sizeof(tre_ike_rsp_mac_cert_t));

	if(!ret)
		LOGD("IKE Request for MAC was a  success \n");
	else
		LOGD("IKE RSA request for MAC Failed (%d)\n", ret);
	return ret;
}


int32_t test_ike_verify_root_cert(void)
{
	int ret = 0;
	const char* cert = "This is a root CA certificate.";

	if (target == FSM99XX) {
		LOGD("Sorry, this feature is not supported for FSM99XX\n");
		return ret;
	}

	req.req_buf.c6.cert_len = strlen(cert) + 1;
	strlcpy((char *)(&req.req_buf.c6.certificate), cert, MAX_DATA_SIZE_BYTES);

	req.req_buf.c6.mac_len = mac_info.mac_len;
	memcpy(&req.req_buf.c6.mac, mac_info.mac, MAX_HASH_SIZE_BYTES);

	ret = tre_ike_verify_CA_certificate (&req.req_buf.c6, &rsp.ret_buf.r6);

	if(!ret)
		LOGD("IKE root certificate verification command was successfuly recieved \n");
	else
		LOGD("IKE root certificate verification command Failed (%d)\n", ret);

	if(!rsp.ret_buf.r6.result)
		LOGD("Root CA certificate was verified\n");
	else
		LOGD("Failed to verify the root CA Certificate (%d)\n", rsp.ret_buf.r6.result);

	return ret;
}

void print_menu_1(void)
{
	LOGD("\t-------------------------------------------------------\n");
	LOGD("\t-------------------------------------------------------\n");
	LOGD("\t 1 -> Use 1024 bit key\n");
	LOGD("\t 2 -> Use 2048 bit key\n");
	LOGD("\t 3 -> Use 1024 bit key in Fuse\n");
	LOGD("\t 4 -> Use 2048 bit key in Fuse\n");
	LOGD("\t 0 -> Exit\n");
	LOGD("\t-------------------------------------------------------\n");

}

void print_menu_2(void)
{
	LOGD("\t-------------------------------------------------------\n");
	LOGD("\t-------------------------------------------------------\n");
	LOGD("\t 1 -> Encrypt IKE Priv Key\n");
	LOGD("\t 2 -> Decrypt and Provision IKE Priv Key in IMEM\n");
	LOGD("\t 3 -> Sign the given buffer using IKE priv keys\n");
	LOGD("\t 4 -> Verify Signature for the given buffer using IKE public key\n");
	LOGD("\t 5 -> Request MAC for root CA Certificate\n");
	LOGD("\t 6 -> Verify root CA Certificate\n");
	LOGD("\t 0 -> Exit\n");
	LOGD("\t-------------------------------------------------------\n");
}

void print_menu_3(void)
{
	LOGD("\t-------------------------------------------------------\n");
	LOGD("\t 1 -> Provision IKE Priv Key in IMEM\n");
	LOGD("\t 2 -> Sign the given buffer using IKE priv keys\n");
	LOGD("\t 3 -> Verify Signature for the given buffer using IKE public key\n");
	LOGD("\t 4 -> Request MAC for root CA Certificate\n");
	LOGD("\t 5 -> Verify root CA Certificate\n");
	LOGD("\t 0 -> Exit\n");
	LOGD("\t-------------------------------------------------------\n");
}

int main (int argc, char *argv[])
{
	int ret;
	char option, cr_ret;
	bool key_in_fuse = false;

	while (1) {
		if ((target = get_target()) == BAD_TARGET)
			exit(-1);

		do {
			print_menu_1();
			_key = NULL;
			LOGD("\t Select an option to proceed: ");
			option = fgetc(stdin);
			cr_ret = fgetc(stdin);
			LOGD("\nRESULTS OUTPUT\n");
			LOGD("************************************************\n\n");
			LOGD("You selected option %c. \n\n", option);
			switch(option) {
			case '0':
				exit(0);
			case '1':
				if (target == FSM99XX) {
					_key = &(_key_1024_fsm99xx);
				}
				else if (target == FSM90XX) {
					_key = &(_key_1024_fsm90xx);
				}
				break;
			case '2':
				if (target == FSM99XX) {
					_key = &(_key_2048_fsm99xx);
				}
				if (target == FSM90XX) {
					_key = &(_key_2048_fsm90xx);
				}
				break;
			case '3':
				if (target == FSM99XX) {
					LOGD("Key in Fuse not supported for Trahira %c\n", option);
				} else if (target == FSM90XX) {
					key_in_fuse = true;
					_key = &(_key_1024_fsm90xx);
				}
				break;
			case '4':
				if (target == FSM99XX) {
					LOGD("Key in Fuse not supported for Trahira %c\n", option);
				} else if (target == FSM90XX) {
					key_in_fuse = true;
					_key = &(_key_2048_fsm90xx);
				}
				break;
			default:
				LOGD("bad option %c\n", option);
				break;
			}
		}while ((_key == NULL) && (key_in_fuse == false));

		if (key_in_fuse == false) {
			do {
				print_menu_2();
				LOGD("\t Select an option to proceed: ");
				option = fgetc(stdin);
				cr_ret = fgetc(stdin);
				LOGD("\nRESULTS OUTPUT\n");
				LOGD("************************************************\n\n");
				LOGD("You selected option %c. \n\n", option);
				switch(option) {
				case '0':
					break;
				case '1':
					ret = test_ike_encrypt_key();
					break;
				case '2':
					ret = test_ike_prov_key(false);
					break;
				case '3':
					ret = test_ike_sign();
					break;
				case '4':
					ret = test_ike_verify_sign();
					break;
				case '5':
					ret = test_ike_req_mac_for_root_cert();
					break;
				case '6':
					ret = test_ike_verify_root_cert();
					break;
				default:
					LOGD("bad option %c\n", option);
					break;
				}
				LOGD("************************************************\n\n");
			}while (option != '0');
		} else {
			do {
				print_menu_3();
				LOGD("\t Select an option to proceed: ");
				option = fgetc(stdin);
				cr_ret = fgetc(stdin);
				LOGD("\nRESULTS OUTPUT\n");
				LOGD("************************************************\n\n");
				LOGD("You selected option %c. \n\n", option);
				switch (option) {
				case '0':
					break;
				case '1':
					ret = test_ike_prov_key(true);
					break;
				case '2':
					ret = test_ike_sign();
					break;
				case '3':
					ret = test_ike_verify_sign();
					break;
				case '4':
					ret = test_ike_req_mac_for_root_cert();
					break;
				case '5':
					ret = test_ike_verify_root_cert();
					break;
				default:
					LOGD("bad option %c\n", option);
					break;
				}
				LOGD("************************************************\n\n");
			}while (option != '0');
		}
	};
	return ret;
}
